import React, { Component } from 'react';

import './App.css';
import Button from "./components/Button/Button";
import ListChat from "./components/ListChat/ListChat";


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: '',
            username: '',
            chats: []
        };
    }
    componentDidMount() {
      this.getMessage()
    };

    handleTextChange =(e) => {
        this.setState({text: e.target.value });
    };

    handleChange =(e) => {
        this.setState({username: e.target.value});
    };

    getMessage =() => {



      const P_URL = 'http://146.185.154.90:8000/messages';
            fetch(P_URL).then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Something went wrong with network request');
            }).then(posts => {

                console.log(posts);

                this.setState({chats: posts});

                let lastTime = posts[posts.length - 1];
                setInterval(() => {
                    fetch('http://146.185.154.90:8000/messages?datetime=' + lastTime.datetime).then(response => {
                        if (response.ok) {
                            return response.json();
                        }
                        throw new Error('Something went wrong with network request');
                    }).then(posts => {

                        if(posts.length !== 0) {

                            const chats = [...this.state.chats];
                            chats.push (...posts);
                            this.setState({chats});
                            lastTime = posts[posts.length - 1];
                        }

                        console.log(posts);
                    });
                }, 2000)


            }).catch(error => {
                console.log(error);
            });
        };

    addPost =() => {
        let formData = new URLSearchParams();
        formData.append('message', this.state.text);
        formData.append('author', this.state.username);

        const P_URL = 'http://146.185.154.90:8000/messages';
        fetch(P_URL, {
            method: 'POST',
            body: formData
        });
    };



    render () {
      return (
          <div className="App">
            <Button
                    handleTextChange={this.handleTextChange}
                    username={this.state.username}
                    handleChange={this.handleChange}
                    text={this.state.text}
                    addPost={this.addPost}
            />

            <ListChat
                      chats={this.state.chats}
               />
          </div>
      );
    };
}

export default App;
