import React from 'react';
import "./Button.css"

const Button = props => {
    return (
        <div className="Button">
            <input
                type="text"
                value={props.text}
                placeholder="chat here..."
                className="form-control"
                onChange={props.handleTextChange}

            />
            <input
                type="text"
                value={props.username}
                placeholder="username"
                className="form-control"
                onChange ={props.handleChange}
            />
            <button onClick={props.addPost}> Add</button>
        </div>
    )
}

export default Button;