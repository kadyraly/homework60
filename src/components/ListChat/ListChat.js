import React from 'react';
import './ListChat.css';

const ListChat = props => {
    return (
        <ul>
            {props.chats.map(chat => {
                return (
                    <div className="chatMessage">
                        <div key={chat.id} className="box">
                            <p>
                                <strong>{chat.author}</strong>
                            </p>
                            <p>{chat.message}</p>
                        </div>

                    </div>

                );
            })}
        </ul>
    )
};

export default ListChat;